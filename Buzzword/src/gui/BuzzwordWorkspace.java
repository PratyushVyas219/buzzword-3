
package gui;

import apptemplate.AppTemplate;
import buzzword.Buzzword;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import static buzzword.BuzzwordProperties.*;

public class BuzzwordWorkspace extends AppWorkspaceComponent {
    public AppTemplate appTemplate;
    public AppGUI gui;
    public Label guiHeadingLabel;
    public Stage current;
    public Button login;
    public Text title;
    public Button newProfile;
    public Scene scene;
    public Group root;
    public Button start;
    public Circle close;
    public Text x;
    public GridPane grid;
    public BuzzwordController controller;
    public Timerr stopwatch;
    public int seconds;
    String[] wordsadded;
    public int targetscoree;
    public Label timer;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS = 330622;

    public BuzzwordWorkspace(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gui = appTemplate.getGUI();
        controller = (BuzzwordController) gui.getFileController();
        layoutGUI();
    }

    public void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        workspace = new VBox();
        Buzzword buzz = (Buzzword) appTemplate;
        makeHomeScreen(buzz.getStage());
    }

    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        //gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        // gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        /*ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));*/

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    @Override
    public void reloadWorkspace() {
    }

    public String[] getWordsadded(){
        return wordsadded;
    }

    public void setHandlers(int a) {
        if(a==1){
            close.setOnMouseClicked(event -> ensure());
            x.setOnMouseClicked(e -> ensure());
        }
        else{
            close.setOnMouseClicked(event -> current.close());
        }
        x.setOnMouseClicked(event -> current.close());
        login.setOnMouseClicked(event -> {
            try {
                controller.loginAlert();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        start.setOnMouseClicked(event -> makeGameSelectScreen());
        newProfile.setOnMouseClicked(event -> {
            try {
                controller.newProfileAlert();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void makeHomeScreen(Stage current) {
        if (current != null) {
            current.close();
        }
        this.current = current;
        root = new Group();

        if(controller.isLoggedin()){
            login.setDisable(true);
        }
        Rectangle rect = new Rectangle(0,100,100,50);
        Text username = new Text("Username: "+ controller.getUsername());
        current.setResizable(false);
        StackPane user = new StackPane();
        user.getChildren().addAll(rect,username);
        StackPane closing = new StackPane();
        x = new Text("X");
        close = new Circle();
        close.setRadius(10);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(650);
        closing.setTranslateY(10);

        HBox buttons = new HBox();
        login = new Button("Login");
        newProfile = new Button("Create new Profile");
        start = new Button("Start Playing");
        title = new Text("Buzzword");
        title.setFont(new Font("Copperplate Gothic Light", 70));
        title.setFill(Color.rgb(51, 107, 135));
        buttons.getChildren().addAll(login, newProfile, start,user);
        root.getChildren().addAll(title, buttons, closing);

        scene = new Scene(root, 700, 700, Color.rgb(144, 175, 197));

        current.setScene(scene);
        title.setTranslateX(160);
        title.setTranslateY(100);
        login.setFont(new Font("Copperplate Gothic Light", 14));
        login.setTranslateX(300);
        login.setTranslateY(400);
        newProfile.setTranslateX(180);
        newProfile.setTranslateY(450);
        newProfile.setFont(new Font("Copperplate Gothic Light", 14));
        start.setTranslateX(40);
        start.setTranslateY(500);
        start.setFont(new Font("Copperplate Gothic Light", 14));

        current.setTitle("Buzzword");
        setHandlers(2);
        current.show();
    }

    public void makeGameSelectScreen() {
        this.current.close();
        this.current = new Stage();
        root = new Group();
        current.setResizable(false);
        StackPane closing = new StackPane();
        x = new Text("X");
        close = new Circle();
        close.setRadius(15);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(650);
        closing.setTranslateY(10);

        Button homeButton = new Button("Home");
        homeButton.setStyle("-fx-background-color: cadetblue");
        homeButton.setFont(Font.font("Copperplate Gothic Light", 20));
        homeButton.setOnMouseClicked(event -> makeHomeScreen(current));

        Text levelselect = new Text("Game Mode Select");
        levelselect.setFont(new Font("Copperplate Gothic Light", 50));
        levelselect.setFill(Color.rgb(51, 107, 135));
        StackPane levels = new StackPane();
        Circle famous = new Circle(80, Color.rgb(51, 107, 135));
        Circle dict = new Circle(80, Color.rgb(51, 107, 135));
        Circle places = new Circle(80, Color.rgb(51, 107, 135));

        Text fame = new Text("Famous People");
        Text dictionary = new Text("Dictionary Words");
        Text placenames = new Text("Place Names");

        fame.setFont(Font.font("Copperplate Gothic Light"));
        dictionary.setFont(Font.font("Copperplate Gothic Light"));
        placenames.setFont(Font.font("Copperplate Gothic Light"));

        famous.setTranslateY(400);
        famous.setTranslateX(80);
        fame.setTranslateY(400);
        fame.setTranslateX(80);
        dict.setTranslateX(280);
        dict.setTranslateY(400);
        dictionary.setTranslateX(280);
        dictionary.setTranslateY(400);
        places.setTranslateX(480);
        places.setTranslateY(400);
        placenames.setTranslateX(480);
        placenames.setTranslateY(400);

        levelselect.setTranslateX(100);
        levelselect.setTranslateY(100);
        famous.setOnMousePressed(event -> famous.setFill(Color.AQUA));
        dict.setOnMousePressed(event -> dict.setFill(Color.AQUA));
        places.setOnMousePressed(event -> places.setFill(Color.AQUA));
        famous.setOnMouseClicked(event -> makeLevelSelectScreen("Famous People"));
        fame.setOnMouseClicked(event -> makeLevelSelectScreen("Famous People"));
        dict.setOnMouseClicked(event -> makeLevelSelectScreen("Dictionary Words"));
        dictionary.setOnMouseClicked(event -> makeLevelSelectScreen("Dictionary Words"));
        places.setOnMouseClicked(event -> makeLevelSelectScreen("Place Names"));
        placenames.setOnMouseClicked(event -> makeLevelSelectScreen("Place Names"));
        levels.getChildren().addAll(famous, fame, dict, dictionary, places, placenames);
        root.getChildren().addAll(levels, levelselect, closing, homeButton);
        scene = new Scene(root, 700, 700, Color.rgb(144, 175, 197));
        current.setScene(scene);
        current.setTitle("Buzzword");
        setHandlers(2);
        current.show();

    }

    public void makeLevelSelectScreen(String category) {
        this.current.close();
        current = new Stage();
        root = new Group();
        Button homeButton = new Button("Home");
        homeButton.setStyle("-fx-background-color: cadetblue");
        homeButton.setFont(Font.font("Copperplate Gothic Light", 20));
        homeButton.setOnMouseClicked(event -> makeHomeScreen(current));
        StackPane closing = new StackPane();
        x = new Text("X");
        close = new Circle();
        close.setRadius(15);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(650);
        closing.setTranslateY(10);
        Button Profile = new Button("View Profile");

        StackPane levels = new StackPane();
        Text levelselect = new Text(category + " Level Select");
        levelselect.setFont(new Font("Copperplate Gothic Light", 30));
        levelselect.setFill(Color.rgb(51, 107, 135));
        int levelcounter=0;
        for (int i = 1; i <= 8; i++) {
            Circle temp = new Circle(20, Color.rgb(51, 107, 135));
            Text text = new Text("" + i);
            temp.setTranslateX(i * 70);
            temp.setTranslateY(500);
            text.setFont(Font.font("Copperplate Gothic Light"));
            text.setTranslateX((i * 70));
            text.setTranslateY(500);
            temp.setOnMousePressed(event -> temp.setFill(Color.AQUA));
            temp.setOnMouseReleased(event -> temp.setFill(Color.rgb(51,107,135)));
            int finalI = i;
            String categ=category;
            if(levelcounter==0) {
                temp.setOnMouseClicked(event -> makeGameplayScreen(finalI, categ));
                text.setOnMouseClicked(event -> makeGameplayScreen(finalI, categ));
            }
            levelcounter++;
            levels.getChildren().addAll(temp, text);
        }
        levelselect.setTranslateX(100);
        levelselect.setTranslateY(100);
        homeButton.setAlignment(Pos.TOP_LEFT);
        root.getChildren().addAll(levelselect, levels, closing, homeButton);
        scene = new Scene(root, 700, 700, Color.rgb(144, 175, 197));
        current.setScene(scene);
        current.setTitle("Buzzword");
        setHandlers(2);
        current.show();
    }

    public void Highlight(String x){
        DropShadow highlight = new DropShadow(20, Color.GOLDENROD);
        for(int row=0;row<4;row++){
            for(int column=0;column<4;column++){
                if(x.equals(lettergrid[row][column])){
                    getNodeFromGridPane(grid,column,row).setEffect(highlight);
                }
            }
        }
    }
    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    public int RandomPlacement() {
        return new Random().nextInt(4)+1;
    }

    public int[] placement() {
        int x = RandomPlacement();
        int[] dum= new int[2];
        if (x == 1){
            dum[0]=0;
            dum[1]=-1;
            return dum;
        }

        else if (x == 2) {
            dum[0]=0;
            dum[1]=1;
            return dum;
        }
        else if (x == 3) {
            dum[0] = -1;
            dum[1] = 0;
            return dum;
        }
        else{
            dum[0]=1;
            dum[1]=0;
            return dum;
        }

    }

    public Scene getScene(){
        return scene;
    }
    public Timerr getTimer(){
        return stopwatch;
    }
    public int getTargetscoree(){
        return targetscoree;
    }

    String[][] lettergrid = new String[4][4];
    public void makeGameplayScreen(int level,String category) {
        int targetscores[]= {400,500,600,700,800,900,1000,1100};
        this.current.close();
        current = new Stage();
        root = new Group();
        HBox rooted = new HBox();
        current.setResizable(false);
        TextField input = new TextField();
        input.setPromptText("Current Guess");
        Text gameplayscreen = new Text("Buzzword " + category+ ": " +level);
        gameplayscreen.setFont(new Font("Copperplate Gothic Light", 60));
        gameplayscreen.setFill(Color.rgb(51, 107, 135));
        StackPane closing = new StackPane();


        grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(100, 150, 500, 10));
        String word = setTargetWord();
        System.out.print(word);
        int row = 0;
        int column = 0;

        boolean taken[][]= new boolean[4][4];
        for(int i =0;i<4;i++){
            for(int a=0;a<4;a++) {
                taken[i][a]=false;
            }
        }

        Text text = new Text(word.substring(0, 1));
        text.setFont(Font.font("Copperplate Gothic Light", 30));
        LetterNode first = new LetterNode(null, null, null, null, text);
        grid.add(first.getCircle(), column, row);
        taken[row][column]=true;
        lettergrid[row][column]=text.getText();

        LetterNode lastnode=first;
        int tempp=1;
        Text next = new Text(word.substring(tempp,tempp+1));
        wordsadded=new String[4];
        int numberword=0;

        for(int i=1;i<16;i++){
            int[] place = placement();
            if(tempp==4){
                wordsadded[numberword]=word;
                word=setTargetWord();
                numberword++;
                System.out.print(word);
                tempp=0;
            }
            if(numberword==2){
                break;
            }
            next = new Text(word.substring(tempp,tempp+1));
            next.setFont(Font.font("Copperplate Gothic Light", 30));
            LetterNode nextnode = new LetterNode(null, null, null, null, next);
            if(row+place[0]>=0 && row+place[0]<4 && column+place[1]>=0 && column+place[1]<4){
            if(taken[row+place[0]][column+place[1]]==false){
                if(place[0]==-1) {
                    taken[row+place[0]][column+place[1]]=true;
                    row--;
                    nextnode = new LetterNode(null, null, null, lastnode, next);
                    lastnode.setTop(nextnode);
                }
                if(place[0]==1){
                    taken[row+place[0]][column+place[1]]=true;
                    row++;
                    nextnode = new LetterNode(null,null,lastnode,null,next);
                    lastnode.setBottom(nextnode);
                }
                if(place[1]==-1){
                    taken[row+place[0]][column+place[1]]=true;
                    column--;
                    nextnode= new LetterNode(null,lastnode,null,null,next);
                    lastnode.setLeft(nextnode);
                }
                if(place[1]==1){
                    taken[row+place[0]][column+place[1]]=true;
                    column++;
                    nextnode = new LetterNode(lastnode,null,null,null,next);
                    lastnode.setRight(nextnode);
                }
                taken[row][column]=true;
                lettergrid[row][column]=next.getText();
                grid.add(nextnode.getCircle(), column, row);
                lastnode=nextnode;
                tempp++;
            }

        }
        else{
                i--;
            }
        }
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       for(int p=0;p<4;p++){
            for(int y=0;y<4;y++){
                if(taken[p][y]==false){
                    int random = new Random().nextInt(26);
                    Text gobble =new Text(alphabet.substring(random,random+1));
                    gobble.setFont(Font.font("Copperplate Gothic Light", 30));
                    LetterNode nextnode = new LetterNode(null, null, null, null, gobble);
                    grid.add(nextnode.getCircle(),y,p);
                }
            }
        }

        VBox screen = new VBox();

        stopwatch = new Timerr();

        VBox information = new VBox();
        timer = new Label("Timer: 60");
        targetscoree=targetscores[level-1];
        Label TargetScore = new Label("Target Score: "+ targetscores[level-1]);
        Label info = new Label("Each letter for a whole word found will give you 100 points");
        info.setFont(Font.font("Copperplate Gothic Light"));
        info.setTextFill(Color.rgb(51, 107, 135));
        Label currentScore = new Label("Current Score: 0");
        timer.setFont(Font.font("Copperplate Gothic Light", 20));
        timer.setTextFill(Color.rgb(51, 107, 135));
        TargetScore.setFont(Font.font("Copperplate Gothic Light"));
        TargetScore.setTextFill(Color.rgb(51, 107, 135));
        currentScore.setFont(Font.font("Copperplate Gothic Light"));
        currentScore.setTextFill(Color.rgb(51, 107, 135));
        TableView table = new TableView();

        TableColumn wordcol = new TableColumn("Words");
        wordcol.setResizable(false);
        wordcol.setStyle("-fx-background-color: #90AFC5");
        TableColumn pointcol = new TableColumn("Points Earned");
        pointcol.setResizable(false);
        pointcol.setStyle("fx-background-color: #90AFC5");

        wordcol.prefWidthProperty().bind(table.widthProperty().multiply(.5));
        pointcol.prefWidthProperty().bind(table.widthProperty().multiply(.5));
        table.getColumns().addAll(wordcol, pointcol);
        table.setMaxWidth(500);

        table.setStyle("-fx-background-color: #336b87");



        close = new Circle();

        close.setRadius(15);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, this.x);
        closing.setTranslateX(1050);
        closing.setTranslateY(10);

        Button pause = new Button("Pause");
        pause.setTranslateY(10);
        pause.setTranslateX(950);
        pause.setOnMouseClicked(event -> Pause());
        information.getChildren().addAll(timer, TargetScore,info, currentScore, table);

        HBox temp = new HBox();
        temp.getChildren().addAll(grid, information);
        screen.getChildren().addAll(gameplayscreen, temp);
        screen.setAlignment(Pos.CENTER);
        input.setTranslateY(540);
        input.setTranslateX(600);
        rooted.getChildren().addAll(screen);
        root.getChildren().addAll(rooted, closing, input, pause);
        scene = new Scene(root, 1100, 700, Color.rgb(144, 175, 197));
        current.setScene(scene);
        current.setTitle("Buzzword");
        setHandlers(1);
        close.setOnMouseClicked(e -> ensure());
        x.setOnMouseClicked(e -> ensure());
        current.show();
        stopwatch.startTime(timer);
       // controller.play();
    }


    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/simple.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(20);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String word = lines.skip(toSkip).findFirst().get();
            if (word.length() < 3) {
                return setTargetWord();
            }
            for (int i = 0; i < word.length(); i++) {
                if (Character.isLetter(word.charAt(i)) != true)
                    return this.setTargetWord();
            }
            return word;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return null;
    }
    public void ensure() {
        ButtonType returned = new ButtonType("return");
        ButtonType closee = new ButtonType("close");
        grid.setVisible(false);
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setResizable(false);
        dialog.setHeight(700);
        dialog.getButtonTypes().setAll(returned,closee);
        dialog.setWidth(700);
        dialog.setTitle("Are you sure?");
        dialog.setHeaderText("You have pressed the X button.");
        dialog.setContentText("If you wish to exit, press close. If you wish to return, press return.");
        Optional<ButtonType> result = dialog.showAndWait();
        if(result.get()==closee){
        current.close();}
        else if(result.get()==returned){
            dialog.close();
        }
        grid.setVisible(true);
    }

    public void Pause() {
        seconds=stopwatch.getTimer();
        grid.setVisible(false);
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setResizable(false);
        dialog.setHeight(700);
        dialog.setWidth(700);
        dialog.setTitle("Game Paused");
        dialog.setHeaderText("Game Paused");
        dialog.setContentText("You have paused the game. Press OK to resume.");
        dialog.showAndWait();
        grid.setVisible(true);
        stopwatch.setTimer(seconds);
        stopwatch.startTime(timer);
    }
}



