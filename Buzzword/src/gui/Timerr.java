package gui;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Prat on 12/11/2016.
 */
public class Timerr {
    Timer timer;
    Text currentTime =  new Text("60");
    int seconds=60;

    public void setTimer(int x){
        seconds=x;
        currentTime= new Text(""+seconds);
    }
    public int getTimer(){
        return seconds;
    }
    public void startTime(Label temp) {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                seconds--;
                Platform.runLater(new Runnable() {
                    public void run(){
                        if(seconds==0){
                            seconds=60;
                            timer.cancel();
                        }
                        String sec= Integer.toString(seconds);
                        currentTime.setText(sec);
                        temp.setText("Timer: "+currentTime.getText());
                }
                });
            }

        };
        timer.schedule(timerTask,1000,1000);
    }
}
