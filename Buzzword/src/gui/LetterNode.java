package gui;


import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;



public class LetterNode {
    public LetterNode left;
    public LetterNode right;
    public LetterNode top;
    public LetterNode bottom;
    public StackPane stack;
    public Text letter;
    public LetterNode(LetterNode a, LetterNode b, LetterNode c, LetterNode d, Text two){
        left=a;
        right=b;
        top = c;
        bottom=d;
        stack = createPane(two);
    }

    public LetterNode(){

    }
    public StackPane createPane(Text b){
        letter=b;
        Circle a = new Circle(20,Color.rgb(51,107,135));
        a.setRadius(50);
        stack= new StackPane();
        Circle finalA = a;
        a.setOnMousePressed(event -> finalA.setFill(Color.AQUA));
        a.setOnMouseReleased(event -> finalA.setFill(Color.rgb(51,107,135)));
        b.setOnMousePressed(event -> finalA.setFill(Color.AQUA));
        b.setOnMouseReleased(event -> finalA.setFill(Color.rgb(51,107,135)));
        stack.getChildren().addAll(finalA,b);
        return stack;
    }


    public void setTop(LetterNode a){
        this.top=a;
    }

    public void setBottom(LetterNode a){
        this.bottom=a;
    }
    public void setLeft(LetterNode a){
        this.left=a;
    }
    public void setRight(LetterNode a){
        this.right=a;
    }


    public StackPane getCircle(){
        return stack;
    }


}
