
package buzzworddata;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class BuzzwordDataFile implements AppFileComponent{
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String DICTIONARY_LEVELS = "DICTIONARY_LEVELS";
    public static final String FAMOUS_LEVELS = "FAMOUS_LEVELS";
    public static final String PLACES_LEVELS = "PLACES_LEVELS";
    public BuzzwordDataFile(){};

    public void saveData(AppDataComponent data, Path filePath){
        BuzzwordData       gamedata    = (BuzzwordData) data;


        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(filePath)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            generator.writeStringField(USERNAME,gamedata.getUsername());
            generator.writeStringField(PASSWORD,gamedata.getPassword());
            generator.writeFieldName(DICTIONARY_LEVELS);
            generator.writeNumber(gamedata.getDictionaryLevels());
            generator.writeFieldName(FAMOUS_LEVELS);
            generator.writeNumber(gamedata.getFamousLevels());
            generator.writeFieldName(PLACES_LEVELS);
            generator.writeNumber(gamedata.getPlaceLevels());

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }



    public  void loadData(AppDataComponent data, Path filePath){}

    public void exportData(AppDataComponent data,Path filePath){}
}
