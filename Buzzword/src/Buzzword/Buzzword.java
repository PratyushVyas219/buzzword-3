package buzzword;


import apptemplate.AppTemplate;
import buzzworddata.BuzzwordData;
import buzzworddata.BuzzwordDataFile;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import gui.BuzzwordWorkspace;
import javafx.stage.Stage;



public class Buzzword extends AppTemplate {
    public static void main(String[] args) {
        launch(args);
    }

    public String getFileControllerClass() {
        return "BuzzwordController";
    }


    public Stage getStage(){
        return current;
    }
    @Override
    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new BuzzwordData(Buzzword.this);
            }


            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new BuzzwordDataFile();
            }


            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new BuzzwordWorkspace(Buzzword.this);
            }
        };
    }
}

