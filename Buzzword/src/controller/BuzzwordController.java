package controller;


import apptemplate.AppTemplate;
import buzzworddata.BuzzwordData;
import com.fasterxml.jackson.databind.ObjectMapper;
import gui.BuzzwordWorkspace;
import javafx.animation.AnimationTimer;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;


public class BuzzwordController implements FileController{
    public AppTemplate appTemplate;
    public String username;
    public String password;
    public boolean loggedin;
    public BuzzwordData gamedata;
    public Path workFile;
    public int score;
    public boolean success;
    public BuzzwordWorkspace workspace;
    public BuzzwordController(AppTemplate apptemplate){
        this.appTemplate= apptemplate;
    loggedin=false;
    gamedata=(BuzzwordData)apptemplate.getDataComponent();
    workspace=(BuzzwordWorkspace)apptemplate.getWorkspaceComponent();
    score = 0;}

    public boolean isLoggedin(){
        return loggedin;
    }
    public String getUsername(){
        return username;
    }

        public void play(){

            AnimationTimer timer = new AnimationTimer() {
                @Override
                public void handle(long now) {
                    workspace.getScene().setOnKeyTyped((KeyEvent event) -> {
                        char guess = event.getCharacter().charAt(0);
                        guess = Character.toUpperCase(guess);
                        if(!(Character.isLetter(guess))){}

                        else{
                            boolean exists=false;
                            int place=0;
                            String[] words = workspace.getWordsadded();
                            for(int i=0;i<workspace.getWordsadded().length;i++){
                               if(Character.toString(guess).equals(words[i].substring(place,place+1))){
                                   exists=true;
                                   place++;
                               }
                            }
                            if(exists){
                                workspace.Highlight(Character.toString(guess));
                            }
                        }

                    });
                    if (workspace.getTimer().getTimer()== 0 || success){
                        stop();}
                }

                @Override
                public void stop() {
                    super.stop();
                }
            };
            timer.start();
        }


        public void loginAlert() throws IOException {
        TextInputDialog dialog= new TextInputDialog();
        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        dialog.setTitle("Login Credentials");
        dialog.setHeaderText("Please enter your login credentials.");
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);
        dialog.getDialogPane().setContent(grid);
        dialog.showAndWait();
        this.username= username.getText();
        this.password= password.getText();
        handleLoadRequest();
        //else{
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Error");
            error.setHeaderText("Incorrect Username or password");
            error.setContentText("You have either entered your username or password incorrectly.");
            error.showAndWait();
       // }
    }

    public void newProfileAlert() throws IOException {
            TextInputDialog dialog = new TextInputDialog();
            TextField username = new TextField();
            username.setPromptText("Username");
            PasswordField password = new PasswordField();
            password.setPromptText("Password");
            dialog.setTitle("New Profile");
            dialog.setHeaderText("Please enter your desired username.");
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));
            grid.add(new Label("Username:"), 0, 0);
            grid.add(username, 1, 0);
            grid.add(new Label("Password:"), 0, 1);
            grid.add(password, 1, 1);
            dialog.getDialogPane().setContent(grid);

            dialog.showAndWait();
            this.username = username.getText();
            this.password = password.getText();
            gamedata.newProfile(this.username, this.password);
           handleSaveRequest();
    }

    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
    }

    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));
        gamedata = (BuzzwordData) appTemplate.getDataComponent();
    }
    @Override
    public void handleNewRequest(){}

    @Override
    public void handleExitRequest() {

    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            ObjectMapper mapper = new ObjectMapper();

            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());

             // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleSaveRequest() throws IOException {
        //try{
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
          //  FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            File savefile = new File(targetPath.toString(),gamedata.getUsername()+".json");
            Files.createFile(savefile.toPath());
         //  filechooser.setInitialDirectory(targetPath.toFile());
         //  filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
          //  String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
          //  String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
           // FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(String.format("%s (*.%s)", description, extension),
          //          String.format("*.%s", extension));
            //filechooser.getExtensionFilters().add(extFilter);

            save(savefile.toPath());
        } else
            save(workFile);
    }
   // catch ()

}
